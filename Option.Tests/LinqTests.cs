﻿namespace Option.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using Optional;

    [TestFixture]
    public class LinqTests
    {
        private class Base : IEquatable<Base>
        {
            public bool Equals(Base other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Id == other.Id;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                var other = obj as Base;
                return other != null && Equals(other);
            }

            public override int GetHashCode()
            {
                return Id;
            }

            public int Id { get; set; }
        }

        private class Derived : Base
        {
        }

        [Test]
        public void SelectAppliesTransformationIfOptionIsSome()
        {
            Option<string> option = "test";
            var result = option.Select(x => x.ToUpperInvariant());
            Assert.IsTrue(result.HasValue);
            Assert.AreEqual("TEST", result.GetOrDefault());
        }

        [Test]
        public void SelectDoesNotApplyTransformationIfOptionIsNone()
        {
            Option<string> option = null;
            var result = option.Select(x => x.ToUpperInvariant());
            Assert.IsFalse(result.HasValue);
        }

        [Test]
        public void SelectManyAppliesTransformationIfOptionIsSome()
        {
            Option<string> option = "te2st";
            var transformation = new Func<string, Option<char>>(s =>
            {
                var digit = s.ToCharArray().FirstOrDefault(char.IsDigit);
                return digit == default(char) ? Option<char>.None : Option.Some(digit);
            });
            var result = option.SelectMany(transformation);
            Assert.IsTrue(result.HasValue);
            Assert.AreEqual('2', result.GetOrDefault());
            result = "test".ToOption().SelectMany(transformation);
            Assert.IsFalse(result.HasValue);
        }

        [Test]
        public void SelectManyDoesNotApplyTransformationIfOptionIsNone()
        {
            Option<string> option = null;
            var transformation = new Func<string, Option<char>>(s =>
            {
                var digit = s.ToCharArray()
                    .FirstOrDefault(char.IsDigit);
                return digit == default(char) ? Option<char>.None : Option.Some(digit);
            });
            var result = option.SelectMany(transformation);
            Assert.IsFalse(result.HasValue);
        }

        [Test]
        public void WhereFiltersOptionIfOptionIsSome()
        {
            var option = Option.From(6);
            var result = option.Where(x => x%2 == 0);
            Assert.IsTrue(result.HasValue);
            Assert.AreEqual(6, result.GetOrDefault());
            result = option.Where(x => x%2 != 0);
            Assert.IsFalse(result.HasValue);
        }

        [Test]
        public void WhereDoesNotFilterOptionIfOptionIsNone()
        {
            var option = Option<int>.None;
            var result = option.Where(x => x%2 == 0);
            Assert.IsFalse(result.HasValue);
        }

        [Test]
        public void AnyReturnsFalseIfOptionIsNone()
        {
            var option = Option<int>.None;
            var result = option.Any(x => x > 5);
            Assert.IsFalse(result);
        }

        [Test]
        public void AnyReturnsTrueIfOptionIsSomeAndPredicatePasses()
        {
            var option = Option<int>.Some(7);
            var result = option.Any(x => x > 5);
            Assert.IsTrue(result);
        }

        [Test]
        public void AnyReturnsFalseIfOptionIsSomeAndPredicateDoesNotPass()
        {
            var option = Option<int>.Some(2);
            var result = option.Any(x => x > 5);
            Assert.IsFalse(result);
        }

        [Test]
        public void AllReturnsTrueIfOptionDoesNotHoldValue()
        {
            var option = Option<int>.None;
            var result = option.All(x => x > 5);
            Assert.IsTrue(result);
        }

        [Test]
        public void AllReturnsTrueIfOptionIsSomeAndPredicatePasses()
        {
            var option = Option<int>.Some(7);
            var result = option.All(x => x > 5);
            Assert.IsTrue(result);
        }

        [Test]
        public void AllReturnsFalseIfOptionIsSomeAndPredicateDoesNotPass()
        {
            var option = Option<int>.Some(2);
            var result = option.All(x => x > 5);
            Assert.IsFalse(result);
        }

        [Test]
        public void ContainsChecksIfOptionContainsGivenElement()
        {
            var option = "test".ToOption();
            var result = option.Contains("test");
            Assert.IsTrue(result);
        }

        [Test]
        public void ContainsCanCheckForDerivedType()
        {
            var option = Option.From(new Base
            {
                Id = 5
            });
            var firstDerived = new Derived
            {
                Id = 6
            };
            var secondDerived = new Derived
            {
                Id = 5
            };

            Assert.IsFalse(option.Contains(firstDerived));
            Assert.IsTrue(option.Contains(secondDerived));
        }

        [Test]
        public void ForEachPerformsActionOnOptionValueIfOptionIsSome()
        {
            int valueLength = 0;
            var option = Option.Some("abc");
            option.ForEach(str => valueLength = str.Length);
            Assert.AreEqual(3, valueLength);
        }

        [Test]
        public void ForEachDoesNotPerformActionIfOptionIsNone()
        {
            int valueLength = 0;
            var option = Option<string>.None;
            option.ForEach(str => valueLength = str.Length);
            Assert.AreEqual(0, valueLength);
        }

        [Test]
        public void QuerySyntaxWithSelectMany()
        {
            var first = Option.Some(5);
            var second = Option<int>.None;
            var third = Option.Some(10);
            var fourth = Option.Some(20);

            var result1 =
                from x in first
                from y in second
                from z in third
                select x + y + z;

            Assert.IsFalse(result1.HasValue);

            var result2 =
                from x in first
                from y in third
                from z in fourth
                select x + y + z;

            Assert.IsTrue(result2.HasValue);

            Assert.AreEqual(35, result2.GetOrDefault());
        }

        [Test]
        public void QuerySyntaxSelectingNullReturnsNone()
        {
            var third = Option.Some(10);
            var fourth = Option.Some(20);

            var result = from x in third
                from y in fourth
                select (string) null;

            Assert.IsFalse(result.HasValue);
        }

        [Test]
        public void QuerySyntaxWithWhere()
        {
            var first = Option.Some(10);
            var second = Option<int>.None;

            var result1 =
                from x in first
                where x > 9
                select x;

            var result2 = from x in first where x < 1 select x;

            Assert.IsTrue(result1.HasValue);
            Assert.AreEqual(10, first.GetOrDefault());

            Assert.IsFalse(result2.HasValue);

            var result3 = from x in second where x > 123 select x;
            Assert.IsFalse(result3.HasValue);
        }

        [Test]
        public void QuerySyntaxWithLetBinding()
        {
            var first = Option.Some(10);
            var second = Option<int>.None;
            var result1 = from x in first
                let temp = x*2
                where temp > 9
                select temp;

            var result2 = from x in first
                let temp = x*3
                where x < 1
                select x;

            var result3 = from x in second
                let temp = x%9
                where temp > 4
                select x;

            Assert.IsTrue(result1.HasValue);
            Assert.AreEqual(20, result1.GetOrDefault());

            Assert.IsFalse(result2.HasValue);
            Assert.IsFalse(result3.HasValue);
        }

        [Test]
        public void ZippingTwoOptionsYieldsResultWhenBothOptionsAreSome()
        {
            var first = Option.From(6);
            var second = Option.From("abc");
            var result = first.Zip(second, (i, s) => new string(Enumerable.Repeat(s, i).SelectMany(x => x).ToArray()));

            Assert.IsTrue(result.HasValue);
            Assert.AreEqual("abcabcabcabcabcabc", result.GetOrDefault());
        }

        [Test]
        public void ZippingTwoOptionsReturnsNoneIfSelectorReturnsNull()
        {
            var first = Option.From(6);
            var second = Option.From("abc");
            var result = first.Zip(second, (i, s) => (string) null);

            Assert.IsFalse(result.HasValue);
        }

        [Test]
        public void ZippingIsEquivalentToSelectManyChainInQuerySyntax()
        {
            var first = Option.From(6);
            var second = Option.From("abc");
            var result = from fst in first
                from snd in second
                select new string(Enumerable.Repeat(snd, fst).SelectMany(x => x).ToArray());

            Assert.IsTrue(result.HasValue);
            Assert.AreEqual("abcabcabcabcabcabc", result.GetOrDefault());

            var result2 = from fst in first
                from snd in second
                select (string) null;
            Assert.IsFalse(result2.HasValue);
        }
    }
}