﻿namespace Option.Tests
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using NUnit.Framework;
    using Optional;

    [TestFixture]
    public class BasicBehaviorTests
    {
        [Test]
        public void GetOrDefaultReturnsDefaultValueIfOptionIsNone()
        {
            var option = Option<int>.None;
            Assert.AreEqual(default(int), option.GetOrDefault());
        }

        [Test]
        public void GetOrDefaultReturnsOptionValueIfPresent()
        {
            var option = Option.From(5);
            Assert.AreEqual(5, option.GetOrDefault());
        }

        [Test]
        public void GetOrElseReturnsElseValueIfOptionIsNone()
        {
            var option = Option<string>.None;
            Assert.AreEqual("abc", option.GetOrElse("abc"));
        }

        [Test]
        public void GetOrElseReturnsOptionValueIfPresent()
        {
            var option = Option.From("def");
            Assert.AreEqual("def", option.GetOrElse("abc"));
        }

        [Test]
        public async Task GetOrElseAsyncStartsTaskIfOptionIsNone()
        {
            var option = Option<string>.None;
            int evaluationCount = 0;
            var defaultValue = new Func<Task<string>>(() => Task.Run(() =>
            {
                Interlocked.Increment(ref evaluationCount);
                return "abc";
            }));
            var result = await option.GetOrElseAsync(defaultValue);
            Assert.AreEqual(1, evaluationCount);
            Assert.AreEqual("abc", result);
        }

        [Test]
        public async Task GetOrElseDoesNotStartTaskIfOptionIsSome()
        {
            var option = Option.From("testing");
            int evaluationCount = 0;
            var defaultValue = new Func<Task<string>>(() => Task.Run(() =>
            {
                Interlocked.Increment(ref evaluationCount);
                return "abc";
            }));
            var result = await option.GetOrElseAsync(defaultValue);
            Assert.AreEqual(0, evaluationCount);
            Assert.AreEqual("testing", result);
        }

        [Test]
        public void GetOrElseEvaluatesFunctionIfOptionIsNone()
        {
            int evaluationCount = 0;
            var func = new Func<string>(() =>
            {
                evaluationCount++;
                return "aaa";
            });
            var option = Option<string>.None;
            var result = option.GetOrElse(func);
            Assert.AreEqual(1, evaluationCount);
            Assert.AreEqual("aaa", result);
        }

        [Test]
        public void GetOrElseDoesNotEvaluateFunctionIfOptionIsSome()
        {
            int evaluationCount = 0;
            var func = new Func<string>(() =>
            {
                evaluationCount++;
                return "aaa";
            });
            var option = Option.From("def");
            var result = option.GetOrElse(func);
            Assert.AreEqual(0, evaluationCount);
            Assert.AreEqual("def", result);
        }

        [Test]
        public void ToStringReturnsNoneIfTheOptionIsNone()
        {
            Assert.AreEqual("None", Option<double>.None.ToString());
        }

        [Test]
        public void ToStringReturnsStringifiedValueIfOptionIsSome()
        {
            Assert.AreEqual("56", Option.From(56).ToString());
        }

        [Test]
        public void ToEnumerableReturnsEmptySequenceIfOptionIsNone()
        {
            Assert.IsEmpty(Option<string>.None.ToEnumerable());
        }

        [Test]
        public void ToEnumerableReturnsOneElementSequenceIfOptionIsSome()
        {
            var result = Option.From(32).ToEnumerable().ToList();

            Assert.IsNotEmpty(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(32, result[0]);
        }

        [Test]
        public void OrElseReturnsAlternativeIfOptionIsNone()
        {
            var option = Option<string>.None;
            var result = option.OrElse("Abc".ToOption);
            Assert.IsTrue(result.HasValue);
            Assert.AreEqual("Abc", result.GetOrDefault());
            result = option.OrElse(Option<string>.None);
            Assert.IsFalse(result.HasValue);
        }

        [Test]
        public void OrElseReturnsOptionItselfIfOptionIsSome()
        {
            var option = Option.From("test");
            var result = option.OrElse("Abc".ToOption);
            Assert.AreEqual("test", result.GetOrDefault());
        }

        [Test]
        public void OrElseEvaluatesFunctionIfOptionIsNone()
        {
            var option = Option<string>.None;
            int evaluationCount = 0;
            var alternative = new Func<Option<string>>(() =>
            {
                evaluationCount++;
                return "def";
            });
            var result = option.OrElse(alternative);
            Assert.AreEqual(1, evaluationCount);
            Assert.AreEqual("def", result.GetOrDefault());
        }

        [Test]
        public void OrElseDoesNotEvaluateFunctionIfOptionIsSome()
        {
            var option = Option.From("test");
            int evaluationCount = 0;
            var alternative = new Func<Option<string>>(() =>
            {
                evaluationCount++;
                return "def";
            });
            var result = option.OrElse(alternative);
            Assert.AreEqual(0, evaluationCount);
            Assert.AreEqual("test", result.GetOrDefault());
        }
    }
}