﻿namespace Option.Tests
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Optional.Extensions.Collections;

    [TestFixture]
    public class CollectionExtensionsTests
    {
        [Test]
        public void RetrievingIndexOutOfRange()
        {
            var list = new List<int> {2, 3, 4};
            var item = list.GetAt(8);
            Assert.IsFalse(item.HasValue);
        }

        [Test]
        public void RetrievingExistingIndex()
        {
            var list = new List<int> {2, 3, 4};
            var item = list.GetAt(1);
            Assert.IsTrue(item.HasValue);
            Assert.AreEqual(3, item.GetOrDefault());
        }

        [Test]
        public void RetrievingExistingIndexWithNullValue()
        {
            var list = new List<string> {"abc", "def", null};
            var item = list.GetAt(2);
            Assert.IsFalse(item.HasValue);
        }

        [Test]
        public void RetrievingValueOfNonExistingKey()
        {
            var dict = new Dictionary<int, string> {{2, "aa"}, {3, "abc"}};
            var item = dict.GetValue(0);
            Assert.IsFalse(item.HasValue);
        }

        [Test]
        public void RetrievingValueOfExistingKey()
        {
            var dict = new Dictionary<int, string> {{2, "aa"}, {3, "abc"}};
            var item = dict.GetValue(3);
            Assert.IsTrue(item.HasValue);
            Assert.AreEqual("abc", item.GetOrDefault());
        }

        [Test]
        public void RetrievingNullValueOfExistingKey()
        {
            var dict = new Dictionary<int, string> {{2, "aa"}, {3, "abc"}, {5, null}};
            var item = dict.GetValue(5);
            Assert.IsFalse(item.HasValue);
        }
    }
}