﻿namespace Option.Tests
{
    using System.Linq;
    using NUnit.Framework;
    using Optional.Extensions.Enumerable;

    [TestFixture]
    public class EnumerableExtensionsTests
    {
        [Test]
        public void FirstOptionReturnsNoneOnEmptySequece()
        {
            var seq = Enumerable.Empty<int>();
            var first = seq.FirstOption();
            Assert.IsFalse(first.HasValue);
        }

        [Test]
        public void FirstOptionReturnsSomeOnNonEmptySequence()
        {
            var seq = new[]
            {
                3, 4, 5
            };
            var first = seq.FirstOption();
            Assert.IsTrue(first.HasValue);
            Assert.AreEqual(3, first.GetOrDefault());
        }

        [Test]
        public void FirstOptionReturnsNoneIfThereIsNoMatchInSequence()
        {
            var seq = new[]
            {
                4, 5, 7, 11
            };
            var first = seq.FirstOption(x => x < 0);
            Assert.IsFalse(first.HasValue);
        }

        [Test]
        public void FirstOptionReturnsSomeIfMatchIsFound()
        {
            var seq = new[]
            {
                4, 5, 7, 11
            };
            var first = seq.FirstOption(x => x > 5);
            Assert.IsTrue(first.HasValue);
            Assert.AreEqual(7, first.GetOrDefault());
        }

        [Test]
        public void LastOptionReturnsNoneOnEmptySequence()
        {
            var seq = Enumerable.Empty<string>();
            var last = seq.LastOption();
            Assert.IsFalse(last.HasValue);
        }

        [Test]
        public void LastOptionReturnsSomeOnNonEmptySequence()
        {
            var seq = new[]
            {
                "abc", "def"
            };
            var last = seq.LastOption();
            Assert.IsTrue(last.HasValue);
            Assert.AreEqual("def", last.GetOrDefault());
        }

        [Test]
        public void LastOptionReturnsNoneIfMatchIsNotFound()
        {
            var seq = new[]
            {
                "abc", "def"
            };
            var last = seq.LastOption(s => s.Length > 10);
            Assert.IsFalse(last.HasValue);
        }

        [Test]
        public void LastOptionReturnsSomeIfMatchIsFound()
        {
            var seq = new[]
            {
                "567", "12345"
            };
            var last = seq.LastOption(s => s.All(char.IsDigit));
            Assert.IsTrue(last.HasValue);
            Assert.AreEqual("12345", last.GetOrDefault());
        }

        [Test]
        public void FirstOptionWillReturnNoneIfNullInstanceIsMatched()
        {
            var seq = new[]
            {
                "abc", null, "def"
            };
            var first = seq.FirstOption(s => s == null);
            Assert.IsFalse(first.HasValue);
        }

        [Test]
        public void LastOptionWillReturnNoneIfNullInstanceIsMatched()
        {
            var seq = new[]
            {
                "abc", null, "def"
            };
            var last = seq.LastOption(s => s == null);
            Assert.IsFalse(last.HasValue);
        }
    }
}