﻿namespace Optional.Extensions.Enumerable
{
    using System;
    using System.Collections.Generic;

    public static class EnumerableExtensions
    {
        public static Option<T> FirstOption<T>(this IEnumerable<T> source, Func<T, bool> selector)
        {
            foreach (var item in source)
            {
                if (selector(item))
                    return item;
            }
            return Option<T>.None;
        }

        public static Option<T> FirstOption<T>(this IEnumerable<T> source)
        {
            using (var iterator = source.GetEnumerator())
            {
                return iterator.MoveNext() ? iterator.Current : Option<T>.None;
            }
        }

        public static Option<T> LastOption<T>(this IEnumerable<T> source, Func<T, bool> selector)
        {
            var last = Option<T>.None;
            foreach (var item in source)
            {
                if (selector(item))
                    last = item;
            }
            return last;
        }

        public static Option<T> LastOption<T>(this IEnumerable<T> source)
        {
            var list = source as IList<T>;
            if (list != null)
            {
                return list.Count == 0 ? Option<T>.None : list[list.Count - 1];
            }

            var last = Option<T>.None;
            foreach (var item in source)
            {
                last = item;
            }
            return last;
        }
    }
}