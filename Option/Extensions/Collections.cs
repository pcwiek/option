﻿namespace Optional.Extensions.Collections
{
    using System.Collections.Generic;

    /// <summary>
    /// Option extensions for collections
    /// </summary>
    public static class CollectionsExtensions
    {
        /// <summary>
        /// Gets the element at the index to an <see cref="Option{T}"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="index">Index of an item to get</param>
        /// <returns><see cref="Option{T}"/> which results in either <c>Some</c> if the Option was created with
        /// appropriate result, <c>None</c> if the index was out of bounds, list was empty or the Option result was <c>None</c></returns>
        public static Option<T> GetAt<T>(this IList<T> list, int index)
        {
            if (index > list.Count - 1 || index < 0 || list.Count == 0)
                return Option<T>.None;
            return Option.From(list[index]);
        }

        /// <summary>
        /// Tries to get a value from a dictionary by key. Alternative to <see cref="IDictionary{TKey, TValue}.TryGetValue"/>
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <returns><see cref="Option{T}"/> - <c>Some</c> if the value exists in the dictionary, <c>None</c> otherwise</returns>
        public static Option<TValue> GetValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            return dictionary.ContainsKey(key) ? Option.From(dictionary[key]) : Option<TValue>.None;
        }
    }
}