﻿namespace Optional
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// Option type
    /// </summary>
    /// <typeparam name="T">The type of the optional instance</typeparam>
    public struct Option<T> : IEquatable<Option<T>>
    {
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(Option<T> other)
        {
            return HasValue.Equals(other.HasValue) && EqualityComparer<T>.Default.Equals(value, other.value);
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <returns>
        /// true if <paramref name="obj"/> and this instance are the same type and represent the same value; otherwise, false.
        /// </returns>
        /// <param name="obj">Another object to compare to. </param><filterpriority>2</filterpriority>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Option<T> && Equals((Option<T>) obj);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is the hash code for this instance.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override int GetHashCode()
        {
            unchecked
            {
                return (HasValue.GetHashCode()*397) ^ EqualityComparer<T>.Default.GetHashCode(value);
            }
        }

        /// <summary>
        /// Tests option value equality 
        /// </summary>
        /// <param name="left">Left hand side option value</param>
        /// <param name="right">Right hand side option value</param>
        /// <returns><c>true</c> if options are equal by value, <c>false</c> otherwise</returns>
        public static bool operator ==(Option<T> left, Option<T> right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Tests option value inequality
        /// </summary>
        /// <param name="left">Left hand side option value</param>
        /// <param name="right">Right hand side option value</param>
        /// <returns><c>true</c> if options are equal by value, <c>false</c> otherwise</returns>
        public static bool operator !=(Option<T> left, Option<T> right)
        {
            return !left.Equals(right);
        }

        private readonly T value;

        private readonly bool hasValue;

        /// <summary>
        /// Indicates whether the option holds a value or not
        /// </summary>
        public bool HasValue
        {
            get { return hasValue; }
        }

        private Option(T value)
        {
            this.value = value;
            hasValue = true;
        }

        /// <summary>
        /// Unwraps the option.
        /// </summary>
        /// <param name="defaultValue"></param>
        /// <returns>Specified, default value if the option is empty, or the option's value if present</returns>
        /// <remarks>It's recommended to use the <see cref="GetOrElse(Func{T})"/> when passing a parameter expression to be evaluated.
        /// In this case, the condition will get evaluated AFTER evaluation of the parameter, which may be costly.</remarks>
        public T GetOrElse(T defaultValue)
        {
            return HasValue ? value : defaultValue;
        }

        /// <summary>
        /// Unwraps the option
        /// </summary>
        /// <param name="defaultValue">Function returning the default value</param>
        /// <returns>The evaluation of the specified function if option is empty, or the option's value if present</returns>
        /// <remarks>Recommended overload. The function returning defaultValue will only get evaluated if
        /// the option is empty.</remarks>
        public T GetOrElse(Func<T> defaultValue)
        {
            return HasValue ? value : defaultValue();
        }

        /// <summary>
        /// Unwraps the option asynchronously
        /// </summary>
        /// <param name="defaultValue">Function returning <see cref="System.Threading.Tasks.Task"/>, which creates the default value</param>
        /// <returns>The evaluation of the specified function if option is empty, or the option's value wrapped in <see cref="Task"/> if present</returns>
        public Task<T> GetOrElseAsync(Func<Task<T>> defaultValue)
        {
            return HasValue ? Task.FromResult(value) : defaultValue();
        }

        /// <summary>
        /// Unwraps the option
        /// </summary>
        /// <returns>The default value for the type if option is empty, or the option's value if present</returns>
        public T GetOrDefault()
        {
            return GetOrElse(default(T));
        }

        /// <summary>
        /// Returns a string that represents current object.
        /// </summary>
        /// <returns>A string representation of wrapped value if there is one, <c><see cref="None"/></c> otherwise</returns>
        public override string ToString()
        {
            return !HasValue ? "None" : value.ToString();
        }

        private static readonly Option<T> none = new Option<T>();

        /// <summary>
        /// Empty option - None value
        /// </summary>
        public static Option<T> None
        {
            get { return none; }
        }

        /// <summary>
        /// Creates an option holding a value.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown when the argument is null. If there's no guarantee that the argument will have a value, use <see cref="From"/> instead</exception>
        /// <param name="value">Value to be wrapped in an option</param>
        /// <returns>A new option holding a value</returns>
        public static Option<T> Some(T value)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            return new Option<T>(value);
        }

        /// <summary>
        /// Creates an option
        /// </summary>
        /// <param name="value">Value to be wrapped in an option</param>
        /// <returns>Either <see cref="None"/> or Some (option holding a value), depending if the value was null or not</returns>
        public static Option<T> From(T value)
        {
            if (value == null)
                return None;

            return Some(value);
        }

        /// <summary>
        /// Applies the specified function to option's value (if the option is non-empty) and yields a new option.
        /// </summary>
        /// <typeparam name="TResult">Type of the result option</typeparam>
        /// <param name="func">Projection function</param>
        /// <returns>A new option after applying the function to current option's value if the current option is non-empty, <see cref="None"/> otherwise.</returns>
        public Option<TResult> Select<TResult>(Func<T, TResult> func)
        {
            return !HasValue ? Option<TResult>.None : Option.From(func(value));
        }

        /// <summary>
        /// Applies a specified function to the option's value and yields a new option if the option is non-empty.
        /// </summary>
        /// <typeparam name="TResult">Type of the result option</typeparam>
        /// <param name="func">Projection function</param>
        /// <returns>A new option if the current option is non-empty, <see cref="None"/> otherwise.</returns>
        public Option<TResult> SelectMany<TResult>(Func<T, Option<TResult>> func)
        {
            return !HasValue ? Option<TResult>.None : func(value);
        }

        /// <summary>
        /// Filters the option by the passed predicate function
        /// </summary>
        /// <param name="func">Predicate function</param>
        /// <returns>Returns the option if the option is non-empty and the value underneath satisfied the predicate, <see cref="None"/>  otherwise.</returns>
        public Option<T> Where(Func<T, bool> func)
        {
            if (HasValue && func(value))
                return this;

            return None;
        }

        /// <summary>
        /// Checks if the value 'exists' inside the option.
        /// </summary>
        /// <param name="func">Predicate function</param>
        /// <returns><code>true</code> if the option is not empty and if it satisfied the predicate, <see cref="None"/> otherwise.</returns>
        public bool Any(Func<T, bool> func)
        {
            return HasValue && func(value);
        }

        /// <summary>
        /// Checks if the option is empty or if the value inside the option satisfies the predicate.
        /// </summary>
        /// <param name="func">Predicate function</param>
        /// <returns><code>true</code> if the option is empty or if the value satisfies the predicate, <see cref="None"/> otherwise.</returns>
        public bool All(Func<T, bool> func)
        {
            return !HasValue || func(value);
        }

        /// <summary>
        /// Tests whether the option contains a given item as element
        /// </summary>
        /// <typeparam name="TDerived">Type of the element</typeparam>
        /// <param name="item">Item to test</param>
        /// <returns><c>true</c> if the option is not empty and <paramref name="item"/> equals current option value.</returns>
        /// <remarks>Uses <code>Equals</code> for equality comparison</remarks>
        public bool Contains<TDerived>(TDerived item) where TDerived : T
        {
            return HasValue && value.Equals(item);
        }

        /// <summary>
        /// Executes a specified action on the option, if the option is non-empty.
        /// </summary>
        /// <param name="action">Action to be executed on the option value</param>
        public void ForEach(Action<T> action)
        {
            if (HasValue)
                action(value);
        }

        /// <summary>
        /// Zips two options together, creating a new option of specified result if both options have a value.
        /// </summary>
        /// <typeparam name="TOther">Other option type</typeparam>
        /// <typeparam name="TResult">Result option type</typeparam>
        /// <param name="other">Other option instance</param>
        /// <param name="resultSelector">Result selector function</param>
        /// <returns><see cref="None"/> if either of the options does not have a value, or option created from combining value of two options otherwise.</returns>
        public Option<TResult> Zip<TOther, TResult>(Option<TOther> other, Func<T, TOther, TResult> resultSelector)
        {
            if (HasValue && other.HasValue)
            {
                return Option.From(resultSelector(value, other.value));
            }

            return Option<TResult>.None;
        }

        /// <summary>
        /// Returns the <paramref name="alternative"/> if the current option is empty. Returns the option itself otherwise.
        /// </summary>
        /// <param name="alternative">Alternative option</param>
        /// <seealso cref="OrElse(Func{Option{T}})"/>
        /// <returns>Current option if it's non-empty, alternative option otherwise</returns>
        public Option<T> OrElse(Option<T> alternative)
        {
            return !HasValue ? alternative : this;
        }

        /// <summary>
        /// Checks whether the current option is empty; if it is, the <paramref name="alternative"/> function is evaluated and the result is returned. Otherwise,
        /// the calling instance is returned.
        /// </summary>
        /// <param name="alternative">Alternative option</param>
        /// <returns>The current option if it's non-empty and the evaluation result of the alternative function otherwise.</returns>
        public Option<T> OrElse(Func<Option<T>> alternative)
        {
            return !HasValue ? alternative() : this;
        }

        /// <summary>
        /// Converts the option to a sequence (<see cref="T:System.Collections.Generic.IEnumerable`1"/>)
        /// </summary>
        /// <returns>One element sequence containing the option's value if the option was non-empty, empty sequence otherwise</returns>
        public IEnumerable<T> ToEnumerable()
        {
            if (!HasValue)
                yield break;

            yield return value;
        }

        /// <summary>
        /// Implicit conversion from value to according option
        /// </summary>
        /// <param name="value">A value</param>
        /// <returns>Empty <c>Option</c> if value is null, non-emtpy <see cref="T:Optional.Option`1"/> otherwise</returns>
        public static implicit operator Option<T>(T value)
        {
            return From(value);
        }
    }

    /// <summary>
    /// Convenience methods for the <see cref="T:Optional.Option`1"/> type
    /// </summary>
    public static class Option
    {
        /// <summary>
        /// Creates an option holding a value.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown when the argument is null. If there's no guarantee that the argument will have a value, use <see cref="From{T}"/> instead</exception>
        /// <param name="value">Value to be wrapped in an option</param>
        /// <returns>A new <see cref="T:Optional.Option`1"/> holding specified value</returns>
        public static Option<T> Some<T>(T value)
        {
            return Option<T>.Some(value);
        }

        /// <summary>
        /// Creates an option
        /// </summary>
        /// <param name="value">Value to be wrapped in an option</param>
        /// <returns>Either <see cref="Option{T}.None"/> or Some, depending if the value was null or not</returns>
        public static Option<T> From<T>(T value)
        {
            return Option<T>.From(value);
        }

        /// <summary>
        /// Applies a specified function to the option's value and yields a new option if the option is non-empty.
        /// This extension method is included to support multiple <c>from</c> clauses in query syntax.
        /// </summary>
        /// <typeparam name="TSource">Source option type</typeparam>
        /// <typeparam name="TIntermediate">Intermediate selection type</typeparam>
        /// <typeparam name="TResult">Result type</typeparam>
        /// <param name="option">Source option</param>
        /// <param name="intermediateSelector">Intermediate selector function</param>
        /// <param name="resultSelector">Result selector functioon</param>
        /// <returns>A new option with specified value if all of the selected options in chain are non-empty, <see cref="Option{T}.None"/> otherwise.</returns>
        public static Option<TResult> SelectMany<TSource, TIntermediate, TResult>(this Option<TSource> option,
            Func<TSource, Option<TIntermediate>> intermediateSelector,
            Func<TSource, TIntermediate, TResult> resultSelector)
        {
            return option.SelectMany(x => intermediateSelector(x).SelectMany(y => From(resultSelector(x, y))));
        }

        /// <summary>
        /// Wraps the specified object in an option. If the object is null, returns <see cref="Option{T}.None"/>, otherwise creates a new option
        /// </summary>
        /// <typeparam name="T">Type of the wrapped value and option itself</typeparam>
        /// <param name="value">Value to be wrapped in an option</param>
        /// <returns>A new <see cref="T:Optional.Option`1"/></returns>
        public static Option<T> ToOption<T>(this T value)
        {
            return From(value);
        }
    }
}